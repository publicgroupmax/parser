using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using UnityEngine.Networking;

public class ParserImage : MonoBehaviour
{
    [SerializeField] private InputField _inputUrl;
    private string _strUrl = "https://www.google.com";
    
    //temp test
    public string _strHtmlCode;
    public List<string> _strImageUrls;
   
    [SerializeField] private Image _image;
    private List<GameObject> _listImages = new List<GameObject>();

    public void Parse()
    {
        if (_inputUrl.text == "") _inputUrl.text = _strUrl;

        foreach (var img in _listImages) Destroy(img.gameObject);
        _strImageUrls = new List<string>();

        StartCoroutine(nameof(ParseRequest));
    }
    
    IEnumerator ParseRequest() 
    {
        UnityWebRequest request = UnityWebRequest.Get(_inputUrl.text);
        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.Success) {
            _strHtmlCode = request.downloadHandler.text;
            
            MatchCollection matches = Regex.Matches(_strHtmlCode, "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            foreach (Match match in matches) {
                string imageUrl = match.Groups[1].Value;

                if (imageUrl.Substring(0, 1) == "/") imageUrl = _inputUrl.text + imageUrl;
                _strImageUrls.Add(imageUrl);
                

                UnityWebRequest imageRequest = UnityWebRequestTexture.GetTexture(imageUrl);
                yield return imageRequest.SendWebRequest();

                if (imageRequest.result == UnityWebRequest.Result.Success) {
                    Texture2D texture = DownloadHandlerTexture.GetContent(imageRequest);

                    GameObject node = Instantiate(_image.gameObject, _image.transform.parent);
                    node.SetActive(true);
                    _listImages.Add(node);
                    
                    Image image = node.GetComponent<Image>();     
                    
                    Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

                    image.sprite = sprite;
                }   
            }
        }
    }




    private void ParseWww()
    {
        WWW www = new WWW(_inputUrl.text);
        while (!www.isDone){}

        _strHtmlCode = www.text;
        _strImageUrls = new List<string>();
        int startIndex = 0;
        while (true)
        {
            startIndex = _strHtmlCode.IndexOf("<img", startIndex);
            if (startIndex == -1)
            {
                break;
            }

            int srcIndex = _strHtmlCode.IndexOf("src=", startIndex) + 5;
            int endIndex = _strHtmlCode.IndexOf("\"", srcIndex);
            string imageUrl = _strHtmlCode.Substring(srcIndex, endIndex - srcIndex);
            _strImageUrls.Add(imageUrl);
            startIndex = endIndex;
        }

        foreach (var img in _listImages) Destroy(img.gameObject);
        foreach (string imageUrl in _strImageUrls) StartCoroutine(LoadImage(imageUrl));
    }

    IEnumerator LoadImage(string imageUrl)
    {
        GameObject node = Instantiate(_image.gameObject, _image.transform.parent);
        node.SetActive(true);
        _listImages.Add(node);
        Image image = node.GetComponent<Image>();
        
        WWW www = new WWW(imageUrl);   
        yield return www;
        
        if (www.texture == null) Debug.LogWarning("No texture");  
        
        Sprite sprite = Sprite.Create(www.texture,new Rect(0, 0, www.texture.width, www.texture.height),new Vector2(0.5f, 0.5f));
        image.sprite = sprite;
    }
}